<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE page [
<!ENTITY gnomeversion "41">
<!ENTITY lastversion "40">
<!ENTITY nextversion "42">
	]>
<page xmlns="http://projectmallard.org/1.0/"
      xmlns:its="http://www.w3.org/2005/11/its"
      xmlns:e="http://projectmallard.org/experimental/"
      type="topic"
      id="developers">
  <info>
    <link type="guide" xref="index#more" group="third"/>
    <desc>New features for those working with GNOME technologies</desc>
    <credit type="author">
      <name its:translate="no">Allan Day</name>
      <email its:translate="no">aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name its:translate="no">Link Dupont</name>
      <email its:translate="no">link@gnome.org</email>
    </credit>
    <credit type="author">
      <name its:translate="no">Matthias Clasen</name>
      <email its:translate="no">matthiasc@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 4.0</p>
    </license>
  </info>

  <title>What’s New for Developers</title>

  <p>Major improvements have been made to the GNOME developer experience during
  the GNOME 41 development cycle. Read on to find out more!</p>

  <section id="dev-docs">
    <title>Improved Documentation</title>

    <p>GNOME 41 is accompanied by an extravaganza of documentation
    improvements! These encompass changes to the GNOME developer website, new
    general developer documentation, improved API docs, and a new version of 
    GNOME's design guidelines.</p>
    
    <section id="website">
      <title>New Docs Site</title>
      <media type="image" its:translate="no" src="figures/developer.png"/>

      <p>Since GNOME 40, GNOME's main developer website has been replaced with
      <link href="https://developer.gnome.org/">a new streamlined portal</link>,
      which provides access to all the GNOME platform docs.</p>

      <p>This is coupled with a <link
      href="https://developer.gnome.org/documentation/">new developer docs site
      </link>, which contains general developer documentation, including:</p>
      
      <list>
        <item><p>A new 
        <link href="https://developer.gnome.org/documentation/introduction.html">introduction to the GNOME platform</link>,
        which includes an overview of included components and services, 
        information on the various programming languages that can be used, and 
        introductions to Builder and Flatpak.</p></item>
        <item><p><link href="https://developer.gnome.org/documentation/guidelines.html">Guidelines</link>
      on general topics such as programming conventions, accessibility and
      localization.</p></item>
        <item><p><link href="https://developer.gnome.org/documentation/tutorials.html">Short tutorials</link>
        on common developer tasks, like how to use notifications, or create a
        menu in GTK.</p></item>
      </list>
    </section>

    <section id="api-docs">
      <title>New API Docs</title>

      <p>Many GNOME libraries are now using a new API docs tool, called 
      <cmd>gi-docgen</cmd>. This produces more accurate and consistent 
      documentation, as well as improved documentation websites.</p>
      
      <p>The primary users of <cmd>gi-docgen</cmd> are GTK and its associated 
      libraries, whose docs can be found at
      <link href="https://docs.gtk.org">docs.gtk.org</link>. This includes API
      docs for
      <link href="https://docs.gtk.org/gtk4/">GTK</link>,
      <link href="https://docs.gtk.org/gdk4/">GDK</link>,
      <link href="https://docs.gtk.org/gsk4/">GSK</link>,
      <link href="https://docs.gtk.org/pango/">Pango</link>,
      <link href="https://docs.gtk.org/gdk-pixbuf/">GdkPixbuf</link>,
      <link href="https://docs.gtk.org/glib/">GLib</link>,
      <link href="https://docs.gtk.org/gobject/">GObject</link>, and
      <link href="https://docs.gtk.org/gio/">GIO</link>.</p>
    </section>

    <section id="hig">
      <title>New Human Interface Guidelines</title>
      <media type="image" its:translate="no" src="figures/hig.png"/>

      <p>GNOME's 
      <link href="https://developer.gnome.org/hig/">design documentation</link>
      has also been expanded and refined during the GNOME 41 development 
      cycle.</p>

      <p>The guidelines have been updated to match contemporary design
      practice. They have also been substantially expanded, with additional
      material on accessibility, UI styling, adaptive UI, navigation structures,
      and more. Virtually all the old content has been rewritten, to make the
      guidance as accessible as possible.</p>

      <p>Finally, the HIG has a new website, which looks better, and is easier
      to navigate and search.</p>

    </section>
  
  </section>

  <section id="builder">
    <title>Better Builder</title>

    <p><app>Builder</app>, the GNOME IDE, has a large collection of enhancements
    for GNOME 41.</p>

    <p>The "find in files" feature, which allows finding and replacing
    strings across an entire project, has been redesigned for GNOME 41. It is
    now located in a persistent section in the bottom panel, which makes it
    more discoverable, and has a new search UI, which makes it easy to browse
    results across a project.</p>
    
    <p><app>Builder</app>'s preexisting support for deploying to connected
    devices has been improved for GNOME 41. This allows building and then
    deploying Flatpak bundles to connected devices, such as mobile devices. It
    now works much more automatically; to learn how to use it, see
    <link href="https://www.jwestman.net/2021/08/06/tutorial-mobile-dev-with-builder.html">James Westman's tutorial</link>.</p>

    <p>For GNOME 41, <app>Builder</app> can now also build and run CMake 
    projects, and it can build projects that use a pure Make build setup in a
    Flatpak environment (thanks to being able to handle <cmd>make-args</cmd> and
    <cmd>make-install-args</cmd>).</p>
    
    <p>Finally, <app>Builder</app> has a new markdown renderer for GNOME 41, which produces
    much better-formatted Markdown previews.</p>

  </section>

  <section id="gtk4">
    <title>GTK 4</title>

    <p>There have been two minor updates to GTK 4 since the GNOME 40 release:
    4.2, and 4.4.</p>
    
    <p>NGL, the new GL renderer for GTK 4, is now the default renderer on Linux,
    Windows and Mac. It has noticable improvements to frames per second, as well
    as power and CPU usage. Input handling has been another area for
    improvements in GTK, with changes in compose and dead key handling.</p>
    
    <p>Other improvements in the 4.2 and 4.4 releases include:</p>

    <list>
      <item><p>GTK Inspector is now enabled by default, to make debugging
      easier.</p></item>
      <item><p>There have been various improvements to GTK 4 on Windows, such 
      as using GL for media playback and improved drag and drop support.
      </p></item>
      <item><p>Emoji data has been updated to CLDR 39.</p></item>
    </list>
    
    <p>The <link href="https://blog.gtk.org/">GTK development blog</link> 
    includes more information about the GTK 
    <link href="https://blog.gtk.org/2021/03/30/gtk-4-2-0/">4.2</link> and
    <link href="https://blog.gtk.org/2021/08/23/gtk-4-4/">4.4</link> releases.
    </p>
    
  </section>

  <section id="libadwaita">
    <title>libadwaita</title>

    <p><link href="https://gnome.pages.gitlab.gnome.org/libadwaita/">libadwaita</link>
    is an in-development companion library for GTK 4, which is working towards
    an initial 1.0 release in the coming months. It will provide the GNOME GTK 
    stylesheet, additional widgets, and convenience functionality for GTK 4 
    GNOME apps. It is the technological successor to
    <link href="https://gnome.pages.gitlab.gnome.org/libhandy/">libhandy</link>
    (which can be used in combination with GTK 3).</p>

    <p><code>libadwaita</code> developments during the GNOME 41 cycle include:
    </p>

    <list>
      <item><p>A substantial amount of API cleanup and code refactoring, as the
      library approaches its 1.0 release.</p></item>
      <item><p>Significant CSS stylesheet changes, including a major
      refactoring, general style updates, and work to support recoloring. The
      latter will facilitate dark mode as well as allowing apps to recolor their
      UIs.</p></item>
      <item><p>The addition of <code>AdwApplication</code>, a new base class for
      apps which reduces repetitive code and handles library initialization.
      </p></item>
      <item><p>Inclusion of unread badges in view switchers.</p></item>
      <item><p><link href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/">API documentation</link>
      is now generated with gi-docgen.</p></item>
    </list>

    <p>Documentation on how to use each of the options provided by
    <code>libadwaita</code> will be included in the Human Interface Guidelines,
    to coincide with the <code>libadwaita</code> 1.0 release.</p>
  </section>

  <section id="gjs">
    <title>GJS</title>

    <p><link href="https://gjs.guide/">GJS</link>, the project providing 
    JavaScript bindings for the GNOME platform, boasts a number of improvements
    in GNOME 41:</p>

    <list>
      <item><p>Memory usage has been reduced by approximately 40 bytes per 
      GObject.</p></item>
      <item><p>The <code>TextEncoder</code> and <code>TextDecoder</code> global
      objects have been added, which replace the older
      <code>imports.ByteArray</code> module.</p></item>
      <item><p>An <code>ignoreCaughtExceptions</code> option has been added to
      the GJS debugger. Enabling this option makes the debugger skip exceptions
      if they are already going to be caught elsewhere in the code.</p></item>
      <item><p>Documentation for new contributors has been updated.</p></item>
    </list>

    <p>GJS 41 also includes a good collection of bug fixes.</p>
  </section>

  <section id="gtk-rs">
    <title>gtk-rs</title>

    <p><link href="https://gtk-rs.org/">Rust support for the GNOME platform
    </link> has made significant progress since GNOME 40:</p>

    <list>
      <item><p>Bindings are now provided for GTK 4 and associated
      libraries.</p></item>
      <item><p>It's now easy to get started with Rust and GTK 4, thanks to the
      new <link href="https://gtk-rs.org/gtk4-rs/stable/latest/book/">GUI
      development with Rust and GTK 4 book</link>. Additionally, the
      <link href="https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template">GTK
      Rust template</link> can also be used as the basis for new GTK 4 Rust
      projects.</p></item>
      <item><p>GTK composite templates are now supported, allowing more
      efficient creation of custom widgets. <link
      href="https://github.com/gtk-rs/gtk4-rs/tree/0.1/examples/composite_template">An
      example</link> shows how this can be used in practice.</p></item>
      <item><p>Aside from these other major changes, there have been many other
      smaller improvements, including better documentation, additional GLib
      macros, simpler subclassing, and more.</p></item>
    </list>

    <p>Read the <link
    href="https://gtk-rs.org/blog/2021/06/22/new-release.html">release
    announcement</link> on the gtk-rs blog for more details.</p>
  </section>

  <section id="flatpak-sdk">
    <title>Flatpak SDK Updates</title>
    <p>The GNOME Flatpak SDK received a number of improvements for GNOME 41:</p>
    
    <list>
      <item><p>The Flatpak SDK is now built with sysprof support enabled, making
      profiling flatpak applications easier.</p></item>
      <item><p><app>zenity</app> and <app>librest</app> were removed from the
      runtime.</p></item>
      <item><p><app>libmanette</app> was added to the runtime, enabling gamepad
      support in WebKit.</p></item>
      <item><p>The base runtime was updated to <code>freedesktop-sdk
      21.08</code>, bringing in newer toolchains (such as GCC 11 and LLVM 12)
      and libraries (such as Mesa 21.2).</p></item>
    </list>
  </section>

  <section id="gnome-os">
    <title>GNOME OS Updates</title>
    <p><link href="https://os.gnome.org/">GNOME OS</link> provides nightly
    development snapshots of a complete GNOME system. While it is primarily
    used for GNOME project QA, it can also be a useful reference for downstream
    distributions and app developers. It has received a variety of updates
    during the GNOME 41 cycle:</p>

    <list>
      <item><p>GNOME OS libraries are now built with sysprof enabled, making
      profiling applications on GNOME OS easier.</p></item>
      <item><p><code>systemd-homed</code> is now included and can be enabled
      manually using <cmd>homectl</cmd>.</p></item>
      <item><p>Support has been added for smartcards and other security
      keys.</p></item>
      <item><p>The <code>openconnect</code>, <code>fortisslvpn</code>,
      <code>vpnc</code> and <code>openvpn</code> VPN backends have been added to
      <app>NetworkManager</app>.</p></item>
      <item><p><link href="https://linuxcontainers.org/">LXC</link> tooling is
      now included.</p></item>
      <item><p>RISC-V was added as a CPU architecture.</p></item>
      <item><p>Mutter is now built with <code>initfd</code> support, enabling
      better XWayland support.</p></item>
      <item><p><app>xdg-desktop-portal-gnome</app> has been added, for 
      GNOME-specific desktop portals.</p></item>
    </list>
      
    <p>Finally, GNOME OS builds are now tested using an openQA instance hosted
    at <link href="https://openqa.gnome.org">openqa.gnome.org</link>. For
    details on how developers can use this openQA instance, see the
    <link href="https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/openqa/OpenQA-for-GNOME-developers">developer documentation</link>.</p>

  </section>

  <section id="thats-not-all">
    <title>That's Not All</title>

    <p>Other improvements for developers include:</p>

    <list>
      <item><p>Tracker is now available for macOS <link
      href="https://github.com/Homebrew/homebrew-core/blob/master/Formula/tracker.rb">via
      Homebrew</link>. Install it by running <cmd>brew install
      tracker</cmd>.</p></item>
      <item><p><app>Devhelp</app> now supports API reference documentation
      generated with <cmd>gi-docgen</cmd>.</p></item>
      <item>
        <p>App developers can request the high-performance power profile when
        running a command using <cmd>powerprofilectl</cmd>. For example:</p>
      <code>powerprofilectl launch --reason "Compiling software" ninja</code>
      </item>
    </list>
  </section>
</page>
